# DESCRIPTION
Implementation of methods for extraction of data from various sites

# SETUP
* Prerequisites: jdk 11+, maven and intellij idea
* Project should be opened in intellij idea an setup with jdk 11. 
* All methods are in class methods.java. Outputs are generated in class outputs.java
* Change the paths in Output.java in .getPage calls to reflect your own paths to html files
* The implementation is designed to run on Linux it should run on mac or windows if you download
* the chrome webdriver for specific platorm from http://chromedriver.chromium.org/downloads
* and put it into the resources folder. Then you should change the System.property in Worker.java 
* to point it to the driver