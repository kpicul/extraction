package implementation;

import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.InputSource;
import us.codecraft.xsoup.Xsoup;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Methods {
    private static JSONObject createJson(Map<String, String> dict){
        JSONObject json = new JSONObject();
        for (String key:dict.keySet()) {
            json.put(key,dict.get(key));
        }
        return json;
    }

    private static void xpathToDictOnce(String xpath, Map dict, Document doc, String description){
        String result = Xsoup.compile(xpath).evaluate(doc).get();
        result = result.replaceAll("<img.*?>", " ");
        result = result.replaceAll("<.*?>", "");
        result = result.replaceAll("\n", " ");
        result = result.replaceAll(" +"," ");
        result = result.replaceAll("<.*?>", "");
        dict.put(description, result.replaceAll("&nbsp;"," "));
    }

    private static void xpathToDictList(String xpath, Map dict, Document doc, String description){
        List<String> result = Xsoup.compile(xpath).evaluate(doc).list();
        List<String> cleanedResult = new ArrayList<String>();
        for (String comment:result) {
            String cleanedComment = comment.replaceAll("<.*?>", "").replaceAll("\n","");
            cleanedResult.add(cleanedComment);
        }
        dict.put(description, cleanedResult);
    }

    private static void regexToDictOnce(String pattern, Map dict, String html, String description){
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(html);
        while(m.find()){
            dict.put(description, m.group(1));
            break;
        }
    }

    private static void regexToDictContent(String pattern, Map dict, String html, String description){
        //html = html.replaceAll("(?s)<script.*?</script>","");
        String content = "";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(html);
        while(m.find()){
            content = m.group(1);
            break;
        }
//        content = content.replaceAll("(?s)<script.*?</script>","");
        content = content.replaceAll("&nbsp;","");
        dict.put(description, content);
    }

    private static void regexToDictList(String pattern, Map dict, String html, String description){
        html = html.replaceAll("(?s)<script.*?</script>","");
        ArrayList<String> list= new ArrayList<String>();
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(html);
        while(m.find()){
            list.add(m.group(1).replaceAll("<.*?>",""));
            //System.out.println(m.group(1)); //debug
        }
        dict.put(description, list);
    }

    private static List<String> getTags(String html){
        Pattern p = Pattern.compile("(<.*?>)(.*?)(<.*?>)");
        Matcher m = p.matcher(html);
        List<String> tags = new ArrayList<String>();
        while (m.find()){
            tags.add(m.group(1));
            tags.add(m.group(2));
            if(!m.group(3).equals("")){
                tags.add(m.group(3));
            }
        }
        return tags;
    }


    private static List<String> getByTags(String html){
        Boolean start = false;
        String paster = "";
        List<String> tree = new ArrayList<String>();
        for(int i = 0; i < html.length(); i++){
            char chr = html.charAt(i);
            //System.out.println(chr);
            if(chr == '<' && !start){
                if(!paster.equals("")){
                    tree.add(paster);
                }
                paster = "";
                start = true;
            }
            else if(chr == '>' && start){
                start = false;
                paster += ">";
                if(!paster.equals("") && !paster.equals(" ")){
                    tree.add(paster);
                }
                paster = "";
            }
            if(chr != '>' && chr != '\n' && chr != '\t'){
                paster += chr;
            }
        }
        return tree;
    }

    private static List<String> textElements(String html){
        String pattern = "(<.*?>+)(.+)(<.*?>+)";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(html);
        List<String> textElements = new ArrayList<String>();
        while(m.find()){
            textElements.add(m.group(1)+m.group(2)+m.group(3));
        }
        return textElements;
    }

    private static Set<String> getExElements(List<String> body1, List<String> body2){
        Set<String> elements = new HashSet<String>();
        for (String el1:body1) {
            for (String el2:body2) {
                if(el1.equals(el2)){
                    elements.add(el1);
                }
            }
        }
        return elements;
    }

    private static Set<String> getSimilar(List<String> body1, List<String> body2){
        Set<String> similar = new HashSet<String>();
        for (String el1:body1){
            for(String el2:body2){
                if(!isSame(el1, el2)){
                    if(isSimilar(el1, el2)){
                        similar.add(getSimilarElements(el1, el2));
                    }
                }
            }
        }
        return similar;
    }
    private static boolean isSame(String a, String b){
        return a.equals(b);
    }

    private static boolean isSimilar(String a, String b){
        List<String> as = getByTags(a);
        List<String> bs = getByTags(b);
        if(as.size() != bs.size()){
            return false;
        }
        for(int i = 0; i < as.size(); i++){
            if(as.get(i).matches("<.*?>") && bs.get(i).matches("<.*?>")){
                if(!as.get(i).equals(bs.get(i))){
                    return false;
                }
            }
        }
        return true;
    }

    private static String getSimilarElements(String a, String b){
        List<String> as = getByTags(a);
        List<String> bs = getByTags(b);
        String sim = "";
        for(int i = 0; i < as.size(); i++){
            if(as.get(i).matches("<.*?>")){
                sim += as.get(i).replaceFirst(">",".*?>");
            }
            else{
                sim += "(.*?)";
                //sim += as.get(i);
            }
        }
        return sim;
    }

    public static JSONObject regular24(String html){
        Map dict = new HashMap();
        String titlePattern = "<h1.*?>(.*?)</h1";
        String summaryPattern = "<.*?summary.*?>(.*?)</div";
        String contentPattern = "<onl-article-body.*?>(.*?)<onl-article";
        String commentPattern = "<.*?comment__body.*?>(.*?)</div";
        String authorPattern = "<.*?q=avtor.*?>(.*?)</a";
        regexToDictOnce(titlePattern, dict, html, "Title");
        regexToDictOnce(summaryPattern, dict, html, "Summary");
        regexToDictContent(contentPattern, dict, html, "Content");
        regexToDictList(commentPattern, dict, html, "Comments");
        regexToDictOnce(authorPattern, dict, html, "Author");
        return createJson(dict);
    }

    public static JSONObject xpath24(String html){
        Map dict = new HashMap();
        Document doc = Jsoup.parse(html);
        String xpathTitle = "//h1[@class=article__title]";
        String xpathSummary = "//div[@class=article__summary]";
        String xpathContent = "//div/onl-article-body";
        String xpathComment = "//div[@class=comment__body]";
        String xpathAuthor = "//div[@class=article__details-main]/a";
        xpathToDictOnce(xpathTitle, dict, doc, "Title");
        xpathToDictOnce(xpathSummary, dict, doc, "Summary");
        xpathToDictOnce(xpathContent, dict, doc, "Content");
        xpathToDictList(xpathComment, dict, doc, "Comments");
        xpathToDictOnce(xpathAuthor, dict, doc, "Author");
        return createJson(dict);
    }

    public static JSONObject regularRtv(String html){
        String html2 = html.replaceAll("\n","").replaceAll("\t","");
        Map dict = new HashMap();
        String titlePattern = "<h1>(.*?)</h1>";
        String authorPattern = "<.*?author-name.*?>(.*?)</div";
        String timePattern = "<.*?publish-meta.*?>(.*?)<br";
        String subtitlePattern = "<.*?subtitle.*?>(.*?)</div";
        String leadPattern = "<.*?lead.*?>(.*?)</p";
        String bodyPattern = "<article.*?>(.*?)</article>";
        regexToDictOnce(titlePattern, dict, html2, "Title");
        regexToDictOnce(authorPattern, dict, html2, "Author");
        regexToDictOnce(timePattern, dict, html2, "PublishedTime");
        regexToDictOnce(subtitlePattern, dict, html2, "SubTitle");
        regexToDictOnce(leadPattern, dict, html2, "Lead");
        regexToDictContent(bodyPattern, dict, html2, "Content");
        return createJson(dict);
    }

    public static JSONObject xpathRtv(String html){
        Map dict = new HashMap();
        Document doc = Jsoup.parse(html);
        String xpathTitle = "//h1";
        String xpathAuthor = "//div[@class=author-name]";
        String publishedTimeXpath = "//div[@class=publish-meta]";
        String xpathLead = "//p[@class=lead]";
        String xpathContent = "//div[@class=article-body]";
        xpathToDictOnce(xpathTitle, dict, doc, "Title");
        xpathToDictOnce(xpathAuthor, dict, doc, "Author");
        xpathToDictOnce(publishedTimeXpath, dict, doc, "PublishedTime");
        xpathToDictOnce(xpathLead, dict, doc, "Lead");
        xpathToDictOnce(xpathContent, dict, doc, "Content");
        return createJson(dict);
    }

    public static JSONObject regexOverstock(String html){
        String html2 = html.replaceAll("\n","").replaceAll("\t","");
        Map dict = new HashMap();
        String titlePattern = "<td valign=\"top\".*?><a.*?><b>(.*?)</b></a><br";
        String listPricePattern = "<td.*?><s>(.*?)</s";
        String pricePattern = "<span class=\"bigred\"><b>(.*?)</b";
        String savingPattern = "<td.*?align=\"left\".*?><span class=\"littleorange\">(\\$[\\d\\,]+\\.\\d+).*?</span";
        String savingPercentPattern = "<td.*?align=\"left\".*?><span class=\"littleorange\">\\$[\\d\\,]+\\.\\d+(.*?)</span";
        String contentPattern = "<span class=\"normal\">(.*?)<br";
        regexToDictList(titlePattern, dict, html2, "Titles");
        regexToDictList(listPricePattern, dict, html2, "ListPrices");
        regexToDictList(pricePattern, dict, html2, "Prices");
        regexToDictList(savingPattern, dict, html2, "Savings");
        regexToDictList(savingPercentPattern, dict, html2, "Saving percents");
        regexToDictList(contentPattern, dict, html2, "Content");
        return createJson(dict);
    }

    public static JSONObject xpathOverstock(String html){
        Map dict = new HashMap();
        Document doc = Jsoup.parse(html);
        String xpathTitle = "//td[@valign=top]/a/b";
        String xpathListPrice = "//tbody/tr/td/s";
        String xpathPrice = "//tbody/tr/td/span[@class=bigred]/b";
        String xpathSaving = "//tbody/tr/td[@align=left]/span[@class=littleorange]";
        String xpathContent = "//tr/td[@valign=top]/span[@class=normal]";
        xpathToDictList(xpathTitle, dict, doc, "Title");
        xpathToDictList(xpathListPrice, dict, doc, "ListPrices");
        xpathToDictList(xpathPrice, dict, doc, "Prices");
        xpathToDictList(xpathSaving, dict, doc, "Savings");
        List<String> savings = new ArrayList<String>();
        List<String> savingPercent = new ArrayList<String>();
        for (String o:((List<String>)dict.get("Savings"))) {
            String[] spl = o.split(" ");
            savings.add(spl[0]);
            savingPercent.add(spl[1]);
        }
        dict.put("Savings", savings);
        dict.put("SavingPercent", savingPercent);
        xpathToDictList(xpathContent, dict, doc, "Content");
        return createJson(dict);
    }

    public static void printChildren(Elements els, String space){
        for (Element el:els) {
            System.out.println(space + el.tagName());
            printChildren(el.children(), space+" ");
        }
    }
    public static String getChildren(Elements els, String space){
        String ret = "";
        for (Element el:els) {
            ret += space + el.tagName() + "\n" + getChildren(el.children(),space + " ");
        }
        return ret;
    }

    public static String roadRunner(String html1, String html2){
        String html1r = html1.replaceAll("\n","").replaceAll("\t","").replaceAll(" +", " ");
        String html2r = html2.replaceAll("\n","").replaceAll("\t","").replaceAll(" +", " ");
        Document doc1 = Jsoup.parse(html1);
        Document doc2 = Jsoup.parse(html2);
        Element body1 = doc1.body();
        Element body2 = doc2.body();
//        String chd = getChildren(els,"");
//        System.out.println(chd);
        List<String> tags1 = getByTags(body1.html());
        List<String> tags2 = getByTags(body2.html());
        String bodyHtml1 = body1.html().replaceAll("(?s)<script.*?</script>", "").replaceAll("<!--.*?>","").replaceAll("(?s)<style.*?</style>", "").replaceAll("(<\\w+)[^>]*(>)", "$1$2");;
        String bodyHtml2 = body2.html().replaceAll("(?s)<script.*?</script>", "").replaceAll("<!--.*?>","").replaceAll("(?s)<style.*?</style>", "").replaceAll("(<\\w+)[^>]*(>)", "$1$2");;
//        for(int i = 0; i < tags2.size(); i++){
//            System.out.println(tags1.get(i)+" : "+tags2.get(i));
//        }
//        String[] tg1 = body1.html().replaceAll("(?s)<script.*?</script>", "").replaceAll("<!--.*?>","").replaceAll("(?s)<style.*?</style>", "").split("\n");
//        String[] tg2 = body2.html().replaceAll("(?s)<script.*?</script>", "").replaceAll("<!--.*?>","").replaceAll("(?s)<style.*?</style>", "").split("\n");
//        for(int i = 0; i < tg2.length; i++){
//            System.out.println(tg1[i]+" : "+tg2[i]);
//        }
        List<String> textElements1 = textElements(bodyHtml1);
//        for (String el:textElements1) {
//            System.out.println(el);
//        }
        List<String> textElements2 = textElements(bodyHtml2);
        Set<String> together = getExElements(textElements1, textElements2);
//        for (String el:together) {
//            System.out.println(el);
//        }
        //Set<String> similarities = getSimilar(textElements1, textElements2);
//        for(String el:similarities){
//            System.out.println(el);
//        }
        List<String> tree = new ArrayList<String>();
        for (String el1:textElements1) {
            for(String el2: textElements2){
                if(isSame(el1, el2)){
                    tree.add(el1.replaceAll(">",".*?>"));
                }
                else if(isSimilar(el1, el2)){
                    tree.add(getSimilarElements(el1, el2));
                }
            }
        }
        Set<String> finalTree = new LinkedHashSet<String>();
        String previous = "";
        for (String el:tree) {
            if(!el.equals(previous)){
                finalTree.add(el);
                //System.out.println(el);
            }
            previous = el;
        }
        String expression = "";
        //for(int i = 0; i < finalTree.size(); i++){
//            //System.out.println(finalTree.get(i).split("[<>]")[1]+"-----------------------------------");
//            if(finalTree.get(i).split("[<>]")[1].equals("li.?*") || finalTree.get(i).split("[<>]")[1].equals("p.*?")){
//                expression += "("+finalTree.get(i)+")+\n";
//            }
//            else{
//                expression += finalTree.get(i)+"\n";
//            }
////            if (i < finalTree.size()-1){
////                if(finalTree.get(i + 1).split("[><]")[1].equals("li.*?")){
////                    expression += "<ul>\n";
////                }
////            }
        //}
        for (String el:finalTree) {
            expression += el+"\n";
        }
        return expression;
    }
}