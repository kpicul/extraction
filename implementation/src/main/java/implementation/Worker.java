package implementation;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Worker {

    private WebDriver driver;

    public Worker(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        this.driver = new ChromeDriver(options);
    }

    public void getPage(String url){
        this.driver.get(url);
    }
    public String getSourceHtml(){
        String source_code = "";
        try {
            WebElement element = driver.findElement(By.xpath("//*"));
            source_code = (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].innerHTML;", element);
        }catch (Exception e){
            System.err.println("Error getting html code: "+e);
            driver.quit();
        }
        driver.quit();
        return "<html>"+source_code+"</html>";
    }


}
