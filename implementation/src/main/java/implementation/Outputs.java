package implementation;

public class Outputs {
    public static void main(String[] args) {
        String t4 = "https://www.24ur.com/novice/slovenija/policistka-si-je-izmislila-prekrske.html";
        Worker t4w = new Worker();
        t4w.getPage(t4);
        String html24 = t4w.getSourceHtml();
        System.out.println("24ur regex1");
        System.out.println(Methods.regular24(html24).toString());
        System.out.println("24ur xpath1");
        System.out.println(Methods.xpath24(html24).toString());
        Worker t42 = new Worker();
        t42.getPage("https://www.24ur.com/novice/svet/slovenski-turisti-na-vrhu-velebita-izobesili-zastavo-nekdanje-jugoslavije.html");
        String html242 = t42.getSourceHtml();
        System.out.println("24ur regex2");
        System.out.println(Methods.regular24(html242).toString());
        System.out.println("24ur xpath2");
        System.out.println(Methods.xpath24(html242).toString());
        String overstockUrl = "file:///media/disk0/FRI/magistrski/iskanje_in_ekstrakcija_podatkov/seminarska2/input/overstock.com/jewelry01.html";
        Worker overstockWorker1 = new Worker();
        overstockWorker1.getPage(overstockUrl);
        String oHtml1 = overstockWorker1.getSourceHtml();
        System.out.println("Overstock regex 1");
        System.out.println(Methods.regexOverstock(oHtml1));
        System.out.println("Overstock xpath1");
        System.out.println(Methods.xpathOverstock(oHtml1));
        String overstockUrl2 = "file:///media/disk0/FRI/magistrski/iskanje_in_ekstrakcija_podatkov/seminarska2/input/overstock.com/jewelry02.html";
        Worker overstockWorker2 = new Worker();
        overstockWorker2.getPage(overstockUrl2);
        String oHtml2 = overstockWorker2.getSourceHtml();
        System.out.println("Overstock regex 2");
        System.out.println(Methods.regexOverstock(oHtml2));
        System.out.println("Overstock xpath 2");
        System.out.println(Methods.xpathOverstock(oHtml2));

        String rtvsloUrl1 = "file:///media/disk0/FRI/magistrski/iskanje_in_ekstrakcija_podatkov/seminarska2/input/rtvslo.si/Audi%20A6%2050%20TDI%20quattro_%20nemir%20v%20premijskem%20razredu%20-%20RTVSLO.si.html";
        Worker rtv1 = new Worker();
        rtv1.getPage(rtvsloUrl1);
        String rtv1Html = rtv1.getSourceHtml();
        System.out.println("regex rtvslo 1");
        System.out.println(Methods.regularRtv(rtv1Html));
        System.out.println("xpath rtvslo 1");
        System.out.println(Methods.xpathRtv(rtv1Html));

        String rtvsloUrl2 = "file:///media/disk0/FRI/magistrski/iskanje_in_ekstrakcija_podatkov/seminarska2/input/rtvslo.si/Volvo%20XC%2040%20D4%20AWD%20momentum_%20suvereno%20med%20najboljs%CC%8Ce%20v%20razredu%20-%20RTVSLO.si.html";
        Worker rtv2 = new Worker();
        rtv2.getPage(rtvsloUrl2);
        String rtv2Html = rtv2.getSourceHtml();
        System.out.println("regex rtvslo 2");
        System.out.println(Methods.regularRtv(rtv2Html));
        System.out.println("xpath rtvslo 2");
        System.out.println(Methods.xpathRtv(rtv2Html));

        System.out.println("Road runner rtvslo");
        System.out.println(Methods.roadRunner(rtv1Html, rtv2Html));

        System.out.println("Road runner 24ur");
        System.out.println(Methods.roadRunner(html24, html242));

        System.out.println("Road runner Overstock");
        System.out.println(Methods.roadRunner(oHtml1, oHtml2));
    }
}
