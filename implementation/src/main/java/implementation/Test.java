package implementation;

import org.json.simple.JSONObject;

import java.util.Map;

public class Test {

    public static void main(String[] args) {
        Worker worker = new Worker();
        worker.getPage("https://www.24ur.com/novice/svet/slovenski-turisti-na-vrhu-velebita-izobesili-zastavo-nekdanje-jugoslavije.html");
        String html = worker.getSourceHtml();
        JSONObject test = Methods.regular24(html);
        System.out.println(test.toString());
        JSONObject testx = Methods.xpath24(html);
        System.out.println(testx.toString());
        //System.out.println(html);
        System.out.println("--------------------------RTVSLO---------------------------");
        Worker worker2 = new Worker();
        worker2.getPage("file:///media/disk0/FRI/magistrski/iskanje_in_ekstrakcija_podatkov/WebPages/rtvslo.si/Audi%20A6%2050%20TDI%20quattro_%20nemir%20v%20premijskem%20razredu%20-%20RTVSLO.si.html");
        html = worker2.getSourceHtml();
        JSONObject testRtv = Methods.regularRtv(html);
        JSONObject testRtvx = Methods.xpathRtv(html);
        System.out.println(testRtv.toString());
        System.out.println(testRtvx.toString());
        //System.out.println(html);
        System.out.println("-------------------------OVERSTOCK-----------------");
        Worker worker3 = new Worker();
        worker3.getPage("file:///media/disk0/FRI/magistrski/iskanje_in_ekstrakcija_podatkov/WebPages/overstock.com/jewelry01.html");
        html = worker3.getSourceHtml();
        JSONObject testOverstock = Methods.regexOverstock(html);
        JSONObject testOverstockXpath = Methods.xpathOverstock(html);
        System.out.println(testOverstock.toString());
        System.out.println(testOverstockXpath.toString());
        Worker rrw1 = new Worker();
        rrw1.getPage("file:///media/disk0/FRI/magistrski/iskanje_in_ekstrakcija_podatkov/WebPages/rtvslo.si/Volvo%20XC%2040%20D4%20AWD%20momentum_%20suvereno%20med%20najboljs%CC%8Ce%20v%20razredu%20-%20RTVSLO.si.html");
        Worker rrw2 = new Worker();
        rrw2.getPage("file:///media/disk0/FRI/magistrski/iskanje_in_ekstrakcija_podatkov/WebPages/rtvslo.si/Audi%20A6%2050%20TDI%20quattro_%20nemir%20v%20premijskem%20razredu%20-%20RTVSLO.si.html");
        String htmlr1 = rrw1.getSourceHtml();
        String htmlr2 = rrw2.getSourceHtml();
        System.out.println(Methods.roadRunner(htmlr1, htmlr2));

    }
}
