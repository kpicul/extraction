function trackOutboundLink(url) {
    ga("send", "event", "outbound", "click", url, {nonInteraction: true});
    return false;
}
