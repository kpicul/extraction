function loadScript(url, callback, id) {
    var script = document.createElement("script")
    script.type = "text/javascript";

    if (id) {
        script.id = id;
    }

    if (script.readyState){  //IE
        script.onreadystatechange = function() {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;

                if (callback && typeof callback === "function") {
                    callback();
                }
            }
        };
    } else {  //Others
        script.onload = function() {
            if (callback && typeof callback === "function") {
                callback();
            }
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

var TAKEOVER = {
    load: function(url, callback, id) {
        var takeoverId = "takeover_" + id;
        return loadScript(url, callback, takeoverId);
    },
    init: function() {
        if (typeof this.onInit === "function") {
            this.onInit();
        }
    },
    destroy: function(id) {
        if (typeof this.onDestroy === "function") {
            this.onDestroy();
            this.onDestroy = null;
            this.onInit = null;
            var scriptToRemove = document.getElementById("takeover_" + id);

            if (scriptToRemove) {
                scriptToRemove.parentNode.removeChild(scriptToRemove);
            }
        }
    },
    cookies: {
        set: function(options) {
            var d = new Date();
            d.setTime(d.getTime() + (1000*options.TTL));
            document.cookie = options.name + "=" + options.name + "; expires=" + d.toUTCString() + "; path=/;";
        },
        get: function(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }

                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }

            return "";
        }
    }
};

try {
  module.exports = {
      TAKEOVER: TAKEOVER,
      loadScript: loadScript
  }
} catch (e) {}
