(function () {
  var c = upScore.config;
  c.version = 1;
  c.mobileDomain = "https://www.24ur.com";
  c.posEndpoint = "//positions.upscore.com";
  c.clickIgnore = { css: [], id: [], tag: [] };
  c.trackArticles = true;
  c.article = ".article__body";
  c.scrollFocus = { article: [".article__header", ".article__details", ".article__body"], landingpage: [".router-content"] };
  c.heatmap = {
    sample: 0.3,
    container: '.box',
    ignore: [[0, 100]],
    heatmapIgnore: [".cookies", ".header--show", ".scroll-top--on", ".callout--toaster.callout--toaster-on", ".close.close--white"],
    resolutionBucket: { mobile: 412, tablet: 994, desktop: 1233 }
  };
  c.bubbleStops = { css: ["grid__main", "focus", "list", "grid__sidebar", "header__main", "ticker"], id: [], tag: [] };
  c.ignoredElements = { css: [], id: [], tag: [] };
  c.positions = [{
    "container": [".voyo__slide", ".splash__item", "onl-news-main>div>div>div>a.card", "onl-focus>div>div>a.card", "onl-other-news>div>div>a.card", "onl-exclusive-main>div>a.news-list__item", "onl-sport-main>div>a.news-list__item", ".card-overlay.card-overlay--ad", ".pager__item", ".card-overlay.card-overlay--sidebar"],
    "urlSelector": ["node"],
    "titleSelector": [],
    "imageSelector": []
  }, {
    "container": [".focus__item", ".ob-dynamic-rec-container"],
    "urlSelector": ["a"],
    "titleSelector": [],
    "imageSelector": []
  }, {
    "container": [".sidebar__box.sidebar__box--bg"],
    "urlSelector": ["no-url"],
    "titleSelector": [],
    "imageSelector": []
  }, {
    "container": [".ticker__wrapper", "div.buttons>a"],
    "urlSelector": ["no-url"],
    "titleSelector": [],
    "imageSelector": [],
    "ribbonClass": "ribbon-wrapper-dot"
  }, {
    "container": [".header__nav-link", ".list__link"],
    "urlSelector": ["node"],
    "titleSelector": [],
    "imageSelector": [],
    "ribbonClass": "ribbon-wrapper-dot"
  }
  ];

  c.index = "com.24ur-multi";
  c.domain = "24ur.com";

  var upScore_hash = function (data) {
    var hash = 0, i, chr;
    if (data === 0) return hash;
    for (i = 0; i < data.length; i++) {
      chr = data.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      hash |= 0;
    }
    return hash;
  };

  c.playerAttachAttempts = 1;
  c.attachPlayerTimer = null;
  c.flowTimer = null;
  c.playerAttached = false;
  var _VIDEOELEMENT = 'onl-video>span>div';
  var _VIDEOTITLE = '.bmpui-label-metadata-description';
  //c.upScoreClient = "PROPLUS";
  var upScoreParticle = null;
  var videoId = null;
  var lastPosition = -1;
  var play_time = 0;

  //get video element, hook up player event handlers and set the interval that will send player info every second
  var attachPlayer = function (videoElement = document.querySelector(_VIDEOELEMENT)) {
    try {
      if (videoElement) {

        c.videoElementExists = true;
        var playerId = videoElement.id;
        var bPlayer = bitmovin.player(playerId);
        bPlayer.addEventHandler(bitmovin.player.EVENT.ON_PLAYING, onPlayEventHandler.bind(bPlayer, videoElement));
        bPlayer.addEventHandler(bitmovin.player.EVENT.ON_PAUSED, onPauseEventHandler.bind(bPlayer, videoElement));
        bPlayer.addEventHandler(bitmovin.player.EVENT.ON_PLAYBACK_FINISHED, onPlayBackFinishedHandler.bind(bPlayer, videoElement));
        c.flowTimer = setInterval(sendPlayingVideoInfo.bind(bPlayer, videoElement), 1000);
        c.playerAttached = true;
      } else {
        c.videoElementExists = false;
        setTimeout(attachPlayer, 1000);
      }
    } catch (e) {
      //if for some reason video element exists, but bplayer object is not available, try 10 times to re attach to video
      if (c.playerAttachAttempts > 20) {
        upScore.event({ videoTracker_attached: 'i:0' });
        upScore.sendWarning("attachPlayer failed", e);
        return;
      }
      c.playerAttachAttempts++;
      setTimeout(attachPlayer, 1000);
    }
  };

  var onPlayBackFinishedHandler = function (videoElement) {
    if (videoId == videoElement.id) { // send only info for current video element
      if (!this.isAd()) {
        var event = { video_stop: 'i:' + parseInt(bPlayer.getDuration()) * 1000 };
        upScore.event(event);
        if (upScoreParticle != null) {
          upScoreParticle.addEvent(event);
        }
      }
    }
  }

  var onPauseEventHandler = function (videoElement) {
    if (videoId == videoElement.id) { // send only info for current video element

      if (!this.isAd()) {
        var currentTime = parseInt(this.getCurrentTime()) * 1000;
        var durationTime = parseInt(this.getDuration()) * 1000;
        if (currentTime === durationTime) {
          var stopEvent = { video_stop: 'i:' + currentTime };
        } else {
          var pauseEvent = { video_pause: 'i:' + currentTime };
        }

        var event = stopEvent || pauseEvent;
        if (upScoreParticle != null) {
          upScoreParticle.addEvent(event);
        }
        upScore.event(event);
      }
    }
  }

  var upScoreReinit = function () {

    var data = { object_id: videoId, object_type: "video", reinitType: "videoTracking", }
    upScore.reinit(data);
  }

  var createNewVideoParticle = function (player) {
    var videoConfig = getVideoMetadata(player)
    var particle_data = { particle_id: 'P_' + videoId, object_type: "video" };
    if (typeof videoConfig.title !== "undefined") {
      particle_data.title = 's:' + videoConfig.title
    } else if (typeof videoConfig.thumbnail !== "undefined") {
      particle_data.video_thumbnail = 's:' + videoConfig.thumbnail
    }

    upScoreParticle = upScore.addParticle(particle_data);
    play_time = 0;
  }

  var onPlayEventHandler = function (videoElement, eventInfo) {
    try {
      var bplayer = this;
      //create a new if iDs don't match
      if (videoElement.id != videoId) {
        videoId = videoElement.id;
        upScoreReinit();
        createNewVideoParticle(bplayer);
      }
      if (!bplayer.isAd()) {

        var time = parseInt(eventInfo.time) * 1000;
        var play_event = { video_start: 'i:' + time }
        var videoConfig = getVideoMetadata(bplayer)

        if (typeof videoConfig.title !== "undefined") {
          play_event.video_name = 's:' + videoConfig.title
        } else if (typeof videoConfig.thumbnail !== "undefined") {
          play_event.video_thumbnail = 's:' + videoConfig.thumbnail
        }

        upScore.event(play_event);
        upScoreParticle.addEvent(play_event);
      }
    } catch (e) {

      upScore.sendWarning("onPlayEventHandler failed", e);
    }

  }
  function getVideoMetadata(player) {
    var videoConfig = player.getConfig();
    var title = videoConfig.source.description;
    var thumbnail = videoConfig.source.poster;
    return { title, thumbnail }
  }

  var retryAttach = function (type) {
    //clearInterval(c.flowTimer);
    c.playerAttachAttempts = 1;
    lastPosition = -1;
    attachPlayer();
  }

  var sendPlayingVideoInfo = function (videoElement) {
    //send info only for video, not ad
    try {
      if (!this.isAd()) {
        var currentTime = parseInt(this.getCurrentTime()) * 1000;
        play_time += 1000; //tick interval time
        if (currentTime != 0 && currentTime != lastPosition) {
          if (videoId == null) {
            upScore.sendWarning("videoID is null, tracking video without play event!", null);
          }
          lastPosition = currentTime;
          var durationTime = parseInt(this.getDuration()) * 1000;
          var completion = durationTime == 0 ? 0 : Math.round((currentTime / durationTime) * 100);
          var event = {
            'video_position': 'i:' + currentTime,
            'video_duration': 'i:' + durationTime,
            'video_completion': 'i:' + completion,
            'play_time': 'i:' + play_time
          };
          upScore.event(event);
          if (upScoreParticle != null)
            upScoreParticle.addEvent(event);
        }
      }
    } catch (e) {
      //bPlayer object was destroyed, se we need to attach to a new videoElement
      clearInterval(c.flowTimer);
      retryAttach();
    }
  }

  var videoAttachedInfo = function (data) {
    if (data.type === "final") {
      if (c.videoElementExists == true) {
        if (c.playerAttached == false) {
          upScore.event({ videoTracker_attached: 'i:0' });
        } else {
          upScore.event({ videoTracker_attached: 'i:1' });
        }
      }
      c.playerAttached = false;
      c.videoElementExists = false;
    }
  }

  setTimeout(attachPlayer, 1000);

  c.getCustomObjectId = function (data) {
    var objectId = data.object_id;

    if (data.object_type === 'landingpage') {
      objectId = document.location.host + upScore_hash(document.location.host + document.location.pathname);
    }

    return objectId;
  }

  c.customDataHandler = function (data) {
    if (data.object_type === 'landingpage') {
      data.object_id = document.location.host + upScore_hash(document.location.host + document.location.pathname);
    }
    videoAttachedInfo(data);
    return data;
  };

  upScore.track();
})();